.DEFAULT_GOAL := help

# The .PHONY directive in a Makefile indicates that a target is not a real file.
# It ensures that the target is always executed, regardless of whether a file with the same name exists.
# In this Makefile, the .PHONY directive is used to declare targets that perform specific tasks and do not generate output files.
# Targets declared as .PHONY will run every time they are invoked, regardless of file existence.
.PHONY: setup test env unset show_env help

## Show available targets and their descriptions.
help:
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
			helpMessage = match(lastLine, /^## (.*)/); \
			if (helpMessage) { \
				split(lastLine, helpArray, "## "); \
				printf "  \033[36m%-20s\033[0m %s\n", $$1, helpArray[2]; \
			} \
		} \
		{ lastLine = $$0 }' $(MAKEFILE_LIST)

# -------------
# Include steps
# -------------
include python.mk
include node.mk

# -----------------
# Environment steps
# -----------------
## Set the environment variables redirecting it to a file like .envrc
env:
	@make python-env node.mk

## Show the environment variables.
show-env:
	@env|sort

# -------------
# General steps
# -------------
## Setup the repository requirements and check that have all the dependencies installed
setup: python-setup node-setup

## Perform all the validation checks in the terraform modules
validate: python-run-pre-commit python-test

## Execute the release
release: node-release

# ---------
# CLI steps
# ---------
## Execute the cli without any parameter
run-local:
	pipenv run python $(CLI_ENTRYPOINT)

## Execute the cli built with the setup.py, the binary compiled
run-compiled:
	pipenv run $(CLI_NAME)
