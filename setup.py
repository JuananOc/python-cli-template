from setuptools import setup, find_packages

setup(
    name="python-cli",
    version="0.1",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "Click",
    ],
    package_data={},
    entry_points="""
        [console_scripts]
        python-cli=src.delivery.console.cli:cli
    """,
)
