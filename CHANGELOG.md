# Changelog

## [1.1.0](https://gitlab.com/JuananOc/python-cli-template/compare/v1.0.2...v1.1.0) (2024-01-26)


### Features

* updated makefile with new approach to make it more modular ([3821cb0](https://gitlab.com/JuananOc/python-cli-template/commit/3821cb0eb20b9aa632f874bd72fcbb133985c545))

## [1.0.2](https://gitlab.com/JuananOc/python-cli-template/compare/v1.0.1...v1.0.2) (2023-10-20)


### Bug Fixes

* delete pretty-format-json so package-lock.json is not uploaded when generating the release ([e9b2707](https://gitlab.com/JuananOc/python-cli-template/commit/e9b27070b0bfea627bc50b9163c600c79f0a2bf2))
* update makefile and release configuration to push the changelog.md ([4f3c5ae](https://gitlab.com/JuananOc/python-cli-template/commit/4f3c5aedb59bd5727b5f4a333d4f79742b5840dd))

## [1.0.2](https://gitlab.com/JuananOc/python-cli-template/compare/v1.0.1...v1.0.2) (2023-10-20)


### Bug Fixes

* update makefile and release configuration to push the changelog.md ([4f3c5ae](https://gitlab.com/JuananOc/python-cli-template/commit/4f3c5aedb59bd5727b5f4a333d4f79742b5840dd))

## [1.0.1](https://gitlab.com/JuananOc/python-cli-template/compare/v1.0.0...v1.0.1) (2023-10-20)


### Bug Fixes

* updated makefile to correctly generate the release ([77294d8](https://gitlab.com/JuananOc/python-cli-template/commit/77294d82a023356042736dcde5b795d43965ed9f))

## 1.0.0 (2023-10-20)


### Features

* create basic structure for python cli template module ([5065776](https://gitlab.com/JuananOc/python-cli-template/commit/5065776aa48ec448ef451584c2ce0628324d6940))
