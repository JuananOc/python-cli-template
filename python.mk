# ----------------
# Python variables
# ----------------
export PIPENV_PIPFILE = ${PWD}/Pipfile
export PY_COLORS ?= '1'

# ----------------
# Custom variables
# ----------------
export PYTHON_VERSION ?= 3.11.4
export VENV_NAME ?= python-cli
export CLI_NAME ?= python-cli
export CLI_ENTRYPOINT ?= main.py
export TEST_PATH ?= test

# ------------
# Python steps
# ------------

python-env:
	@echo '# Pipenv'
	@echo 'export PIPENV_PIPFILE=${PIPENV_PIPFILE}'
	@echo 'export PY_COLORS=${PY_COLORS}'
	@echo ''
	@echo 'export PYTHON_VERSION=${PYTHON_VERSION}'
	@echo 'export VENV_NAME=${VENV_NAME}'
	@echo 'export CLI_NAME=${CLI_NAME}'
	@echo 'export CLI_ENTRYPOINT=${CLI_ENTRYPOINT}'
	@echo 'export TEST_PATH=${TEST_PATH}'
	@echo 'export GITLAB_TOKEN=${GITLAB_TOKEN}'
	@echo

python-setup:
	@echo "Ensure Python installed"
	@echo "-----------------------"
	@echo "Python version: $(shell python3 --version)"
	@echo "Pipenv version: $(shell pipenv --version)"
	@echo
	@echo "Setup Pipfile"
	@echo "--------------"
	@pipenv install --dev
	@echo
	@echo "Pre-commit version:"
	@echo "-------------------"
	@pipenv run pre-commit --version
	@echo
	@echo "Setup the pre-commit"
	@echo "--------------------"
	@pipenv run pre-commit install-hooks
	@pipenv run pre-commit install
	@pipenv run pre-commit install --hook-type commit-msg
	@echo

python-run-pre-commit:
	@echo "Run pre commit tasks"
	@echo "--------------------"
	@pipenv run pre-commit run --all-files
	@echo

python-clean:
	@echo "Clean Virtual environment"
	@echo "-------------------------"
	pipenv --rm
	@echo

python-test:
	@pipenv run python -m unittest discover -s $(TEST_PATH) -p 'test_*.py'
