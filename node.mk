# --------------
# Node variables
# --------------

# ----------------
# Custom variables
# ----------------
export GITLAB_TOKEN ?= ""

# ----------
# Node steps
# ----------
node-env:
	@echo '# Node'
	@echo 'export GITLAB_TOKEN=${GITLAB_TOKEN}'
	@echo

node-setup:
	@echo "Setup the release process"
	@echo "-------------------------"
	@echo "NPM version: $(shell npm --version)"
	@echo "NPX version: $(shell npx --version)"
	@npm install -D semantic-release@v22.0.5 @semantic-release/changelog conventional-changelog-conventionalcommits@v6.1.0 @semantic-release/gitlab@v12.0.6 @semantic-release/git@10.0.1
	@echo

node-release:
	npx semantic-release --no-ci
