# python-cli-template

Template setup for creating python CLI

## Description
This is a template to create Python Command Line Interfaces with all the setup required for:

- Managing the virtual environment with `Pipenv`.
- Ensure a minimun code quality using `Pre commit hooks` adapted to validate and review Python code.
- Create a setup.py file to generate the binary compiled.

## Installation

To make adapt it to your own needs, need to perform the following manual steps:

1. Decide the name of your cli tool. For that you will need to do the following changes:
  - In `Pipfile` edit the `python-cli`.
  - In `python.mk` edit the `CLI_NAME` and `VENV_NAME` definition in the top to use the name at default.
  - In `setup.py` edit the `name` variable.
  - In `setup.py` edit the `console_script` section changing the left side `python-cli`.

2. Setup the project environment variables. Usage of [direnv](https://github.com/direnv/direnv) is recommended

```shell
make env > .envrc
source .envrc # can use `direnv allow`

make setup
```

3. Run the cli

```shell
make run-local    # run the cli based on the local changes.
make run-compiled # run the cli compiled in local.
```

4. Create the code for your CLI.

5. Validate the code before pushing it.

```shell
make validate
```

6. Push the code.

7. Generate the release. The release number should match the `setup.py` version

```shell
make release
```

## Usage

To develop you can follow the next guidelines:

- The package `src.delivery.console` where we should put all the code relative to click implementation defining what our cli can do.

> Changes on this package needs to be also applied in the setup.py as there we make reference to the Click entrypoint.

- Inside the `src.core` package we can create the packages that have the logic agnostic to the CLI.

- Inside the `test` folder we have the packages for test

> NOTE: is recommended to use the Makefile as what your repository can do, so you can add here the future actions your cli can do.


## Roadmap

- [ ] Add default configuration to generate releases.
- [ ] Make the setup more automatic.
